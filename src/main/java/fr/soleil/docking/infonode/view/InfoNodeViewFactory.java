/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.infonode.view;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JComponent;

import net.infonode.docking.RootWindow;
import net.infonode.docking.util.DockingUtil;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.infonode.InfoNodeDockingManager;
import fr.soleil.docking.view.AbstractViewFactory;
import fr.soleil.docking.view.IView;

/**
 * InfoNode implementation of {@link AbstractViewFactory}
 * 
 * @author girardot
 */
public class InfoNodeViewFactory extends AbstractViewFactory {

    public InfoNodeViewFactory() {
        super();
    }

    @Override
    protected IView createView(String title, Icon icon, Component component, Object id) {
        return new InfoNodeView(title, icon, component, id);
    }

    @Override
    protected void updateViewForDockingArea(IView view, JComponent dockingArea) {
        if ((view instanceof InfoNodeView) && (dockingArea instanceof RootWindow)) {
            InfoNodeView infoNodeView = (InfoNodeView) view;
            RootWindow rootWindow = (RootWindow) dockingArea;
            infoNodeView.getWindowProperties().getTabProperties().getNormalButtonProperties()
                    .getCloseButtonProperties().setVisible(false);
            infoNodeView.getWindowProperties().getTabProperties().getHighlightedButtonProperties()
                    .getCloseButtonProperties().setVisible(false);
            DockingUtil.addWindow(infoNodeView, rootWindow);
        }
    }

    @Override
    public ADockingManager generateDockingManager() {
        return new InfoNodeDockingManager(this);
    }

}
